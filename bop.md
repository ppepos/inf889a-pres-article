---
title: "Block Oriented Programming: Automating Data-Only Attacks"
subtitle: "Présentation d'article par Philippe Pépos Petitclerc"
author: "Ispoglou, Jaeger, AlBassam, Payer"
date: 2018
output:
  beamer_presentation:
    slide_level: 1
---

# Introduction et contexte

## Exploitation binaire moderne

### Exploitation binaire

 - Contrôler l'exécution d'un programme (binaire)
 - Exécuter du code arbitraire (souvent)

### ... moderne

 - Systèmes de protections mémoires
   - Data Execution Prevention (DEP)
   - Address Space Layout Randomization (ASLR)
   - Canary
 - ... modernes
   - Control-Flow Integrity (CFI)
   - Shadow Stacks

## Histoire - Exploits binaires de base

 - Contrôler l'exécution du programme
   - Primitive d'écriture sur pointeur d'instruction (IP)
     - Écriture sur un pointeur de fonction
     - Adresse de retour sauvegardée sur la pile
 - Écrire les instructions souhaitées (shellcode) en mémoire (pile, tas ou autre)
 - Faire exécuter les instructions injectées

## Histoire - Data Execution Prevention

### Data Execution Prevention (DEP) ou NX

 - Marquer des pages mémoires (de données) comme étant non exécutables (NX)
   - Pile
   - Tas
 - ~~Injection de code binaire~~

## Histoire - Ret2Libc

 - Idée de réutilisation de code
 - Construire des cadres d'appels (arguments à des fonctions) sur la pile puis appeler une fonction cible

 ~~`$rip = $rsp`~~

`arg0 = "/bin/sh"`

 `$rip = system`

## Histoire - Return-Oriented Programming (ROP)

 - Réutilisation de code (Niveau 2)
 - Trouver des *gadgets* dans le code existant
   - Instruction souhaitée
   - Suivie d'un `ret`
   - `mov rax, 16; ret`

 - Placer l'adresse des *gadgets* sur la pile
   - À partir de l'emplacement de l'adresse de retour sauvegardée
   - Le `ret` de chaque *gadget* redirigera l'exécution du programme à la prochaine instruction souhaitée
   - Et ainsi de suite...

## Control-Flow Integrity

 - Construire un CFG du programme
 - Dynamiquement (runtime) valider les branchements indirects
   - Début de noeuds du CFG
 - Protèges les arcs ~en avant~ (*Forward Edges*)

### CFG

 - Analyse statique pour le construire
 - Sur-approximation des destinations

### Attaques résiduelles

 - Abuser de l'imprécision du CFG
 - *Data-Only Attacks*

## Shadow Stack

 - Une deuxième pile
 - En zone mémoire protégée
 - Contient seulement les adresses de retour
 - Valider les retours à l'exécution
   - Adresse du retour du Shadow Stack est la même que celle de la pile
 - Protèges les arcs ~en arrière~ (*Backward Edges*)


## Attaques par données (*Data-Only Attacks*)

 - Cibler uniquement les données
 - Ne pas tenter de corrompre le flot de l'exécution
   - Respecter les contraintes du CFI et de la Shadow Stack

### Exemples:

 - Contrôler les arguments d'un appel à execve
 - Contrôler la valeur d'une variable `is_admin`

# Programmation orientée-bloc (*Block-Oriented Programming*, BOP)

## Contributions

 - SPloit Language (SPL)
   - Langage d'abstraction pour représenter des *payloads*

 - Block-Oriented Programming Compiler (BOPC)
   - Compilateur d'exploit BOP
   - Module de traçage
   - => Génère des exploits concrêts à partir de la définition SPL


## Suppositions

 - Programme comporte une vulnérabilité de corruption mémoire
 - Primitive de lecture arbitraire en mémoire... pour ASLR (ARP)
 - Primitive d'écriture arbitraire en mémoire (AWP)
 - Point d'entré
   - Adresse où le programme se rend après chaque AWP

## Conception

### Deux concepts clés

 - Programmation orientée-bloc
   - Exécuter l'effet voulu en réutilisant des séquences de blocs de base du programme
 - Sommaire de contraintes de bloc (*Block Constraint Summary*)
   - Représentation des effets d'un bloc de base

### *BOP-Chain*

 - Séquence de blocs de base du programme qui exécutent le payload souhaité
   - Construit de *BOP-gadgets*

## BOP-Gadget

 - Bloc fonctionnel
   - Bloc ciblé pour sont effet
 - Blocs régulateurs (*Dispatcher Blocks*)
   - Bloc pour lier le bloc fonctionnel au bloc fonctionnel suivant
   - Ne doit pas porter d'effet destructeur

## BOP-Gadget

![BOP-Gadget](res/figure3.png)

## SPL et l'expression d'un payload

### SPL

 - Turing Complete
 - Indépendant de l'architecture
 - ~ Haut-niveau ~
 - APIs pour le système d'exploitation
 - Modèle de registres et de mémoire virtuels

![Payloads SPL](res/table1.png){height=50%}


## Déterminer la séquence de blocs fonctionnels

 - Comparer chaque bloc à chaque instruction SPL
   - Trouver des blocs qui effectuent chaque instruction SPL
 - Instructions machines versus instructions SPL

### Sommaire de contraintes de bloc

 - Isoler et exécuter symboliquement chaque bloc
 - Le système de contrainte résultant est comparé sémantiquement à la représentation des instruction SPL

![Comparaison sémantique des instructions aux instructions SPL](res/table3.png)

## Déterminer les séquences de blocs régulateurs

 - Exécution concolique (Concrète + Symbolique)
   - Exécution symbolique sur une trace précise
   - À partir des blocs fonctionnels et du BOP-Chain en construction

## Synthétiser un exploit

Si l'algorithme retourne une solution:

 - L'ensemble de contraintes retournée représente une configuration de la mémoire qui mène à l'exécution du payload
 - Concrétisation du système de contraintes (SMT Solver)
   - Attention, la concrétisation doit se faire **pendant** l'exécution
     - Dépend des états intermédiaires de certaines variables
 - BOPC retourne une séquence d'écritures (adresse, valeur, taille) à effectuer pour exécuter le payload

# Évaluation

## Résultats

![Résultats de l'exécution de payloads SPL](res/table6.png)
