all: bop.pdf report.pdf

bop.pdf: bop.md
	pandoc -r markdown -w beamer -o bop.pdf bop.md --slide-level=2

bop.md:

report.pdf: report.md
	pandoc -r markdown -w latex --filter pandoc-citeproc -o report.pdf report.md

report.md:


clean:
	rm *.pdf
