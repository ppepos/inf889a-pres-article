---
title: "Résumé d'article (INF889A)"
author: "Philippe Pépos Petitclerc"
date: 2020-04-30
bibliography: article.bib
nocite: |
  @ispoglou_block_2018
---

L'adoption de nouveaux mécanismes de protections mémoire qui tentent de contrer
les attaques de réutilisation de code comme le *Return-Oriented Programming*
(*ROP*) mènent les experts à explorer de nouvelles stratégies d'attaques. Les
mécanismes de protection que les auteurs cherchent à contourner dans l'article
sont ceux qui vérifient l'intégrité du flot de contrôle du programme à
l'exécution. *Control-Flow Guard* et *Control-Flow Integrity* valident les
branchements avants (*Forward Branches*) alors que les piles auxiliaires
(*Shadow Stacks*) valident l'intégrité des branchements arrières (*Backward
Branches*). Parmi les attaques résiduelles, les auteurs ont choisis d'explorer
les attaques par données.

Ispoglou et al. introduisent deux outils: *Sploit Lang* (*SPL*), un langage de
modélisation *payload*, et *BOPC*, un compilateur d'exploits orientés blocs.
*BOPC* prend en entrée un *payload* *SPL* puis tente de trouver une séquence
d'écritures en mémoire qui le concrétisera. La solution nécessite donc que le
logiciel attaqué porte une primitive d'écriture arbitraire qui peut être
appelée plusieurs fois (afin de pouvoir effectuer les écritures en mémoires
requises pour concrétiser le *payload*).

![Exemple de *payload* *SPL*](res/table1.png)

Les *payloads* orientés-blocs sont constitués de séquences de *BOP Gadgets*.
Chacun visant à exécuter une partie de la logique du *payload*. En chaînant
tous les *BOP Gadgets*, on exécute toute la logique du *payload*. Chaque gadget
est construit en identifiant un **bloc fonctionnel** qui est ciblé pour les
instructions qu'il exécute (le bloc fait quelque chose de désirable pour la
construction du *payload*) et de **blocs régulateurs** ou de **transition** qui sont
responsable de lier un bloc fonctionnel au suivant sans défaire le travail que
ce dernier accomplit pour la construction du *payload*. Afin de choisir les blocs,
les auteurs reposent sur le concepte de sommaire de bloc qui, sans entrer dans
les détails techniques, est obtenu en exécutant symboliquement chacun des
blocs.

![Structure d'un *Bop Gadget*](res/figure3.png)

La validation de la stratégie passe par la tentative d'automatiquement
construire des exploits pour certaines applications vulnérables. Les auteurs
ont principalement choisis des applications de type serveur puisqu'il exposent
plus souvent les primitives requises pour tenter l'exploitation. Ils évaluent
leur solution en tentant de synthétiser des exploits pour des *payloads* qui
changent la valeur d'un  ou plusieurs registres, font une lecture en mémoire,
font une écriture en mémoire, impriment une valeur, bouclent de différentes
façons ou encore exécutent des commandes arbitraires. La majeure partie des
exploits tentés sont très simples (parfois l'effet d'une seule instruction), et
à mes yeux, ce ne sont pas des candidats pertinents pour la validation de la
solution. Par contre, ceux qui traffiquent suffisant l'exécution pour effecteur
des boucles, des branchements conditionnels ou des appels systèmes sont
impressionnants. Malheureusement, ils ne parviennent qu'à y arriver sur la
moitié de leurs cas de test.

# Bibliographie
